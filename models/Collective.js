const mongoose = require("mongoose");
const slugify = require("slugify");

const CollectiveSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Please add a name"],
      unique: true,
      trim: true,
      maxlength: [50, "Name cannot be more than 50 characters"],
    },
    slug: String,
    description: {
      type: String,
      required: [true, "Please add a description"],
      maxlength: [500, "Description cannot be more than 500 characters"],
    },
    website: {
      type: String,
      match: [
        /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/,
        "Please use a valid URL with HTTP or HTTPS",
      ],
    },
    photo: {
      type: String,
      default: "no-photo.jpg",
    },
    createdAt: {
      type: Date,
      default: Date.now,
    },
    user: {
      type: mongoose.Schema.ObjectId,
      ref: "User",
      required: true,
    },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

// Get slug for collective
CollectiveSchema.pre("save", function (next) {
  const collectiveID = this._id;
  this.slug = slugify(collectiveID.toString());
  next();
});

// Cascade delete posts & comments if a collective is deleted
CollectiveSchema.pre("remove", async function (next) {
  console.log(`Posts and comments being removed from collective ${this._id}`);
  await this.model("Post").deleteMany({
    collective: this._id,
  });
  await this.model("Comment").deleteMany({
    collective: this._id,
  });
  next();
});

// Reverse populate with virtuals for posts
CollectiveSchema.virtual("posts", {
  ref: "Post",
  localField: "_id",
  foreignField: "collective",
  justOne: false,
});

module.exports = mongoose.model("Collective", CollectiveSchema);
