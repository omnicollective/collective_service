const jwt = require('jsonwebtoken');
const asyncHandler = require('./async');
const ErrorResponse = require('../utils/errorResponse');
const User = require('../models/User');

// Protect routes
exports.protect = asyncHandler(async (req, res, next) => {
    let token;

    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
        token = req.headers.authorization.split(' ')[1];
        console.log(token);
    } 
    else if (req.cookies.token) {
        token = req.cookies.token;
    }

    // Make sure token exists
    if (!token) {
        next(
            new ErrorResponse(`Not authorized to access this route.`,401)
        );
    }
    try {
        // verify token
        // we need to extract the payload
        // which will look like this:
        // { id: 1, iat:xxx, exp }
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        req.user = await User.findById(decoded.id);
        next();
    } catch (err) {
        next(
            new ErrorResponse(`Not authorized to access this route`,401)
        );
    }
});

// grant access to specific roles
exports.authorize = (...roles) => {
    return (req, res, next) => {
        if (!roles.includes(req.user.role)) {
            next(
                new ErrorResponse(`User role ${req.user.role} is not authorized to access this route`,403)
            );
        }
        next();
    }
}