const path = require('path');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Collective = require("../models/Collective");

// @desc    Get all collectives
// @route   GET /api/collective-service/v1/collectives
// @access  Public
exports.getCollectives = asyncHandler(async (req, res, next) => {
  res
    .status(200)
    .json(res.advancedResults);
});

// @desc    Get single collective
// @route   GET /api/collective-service/v1/collectives/:id
// @access  Public
exports.getCollective = asyncHandler(async (req, res, next) => {
  const collective = await Collective.findById(req.params.id);

  if (!collective) {
    return next(
      new ErrorResponse(`Collective not found with id of ${req.params.id}`, 404)
    );
  }

  res.status(200).json({
    success: true,
    data: collective,
  });

});

// @desc    Create new collective
// @route   POST /api/collective-service/v1/collectives
// @access  Private
exports.createCollective = asyncHandler(async (req, res, next) => {

  // add user to req.body
  req.body.user = req.user.id

  // check for published bootcamps
  const publishedCollective = await Collective.findOne({ user: req.user.id });

  // if user is not admin they can only add one collective
  if (publishedCollective && req.user.role !== 'admin') {
    return next(
      new ErrorResponse(`The user with ID ${req.user.id} has already published a collective`, 400)
    );
  }

  const newCollective = await Collective.create(req.body);

  res.status(201).json({
    success: true,
    data: newCollective,
  });

});

// @desc    Update collective
// @route   PUT /api/collective-service/v1/collectives/:id
// @access  Private
exports.updateCollective = asyncHandler(async (req, res, next) => {
  let collective = await Collective.findById(req.params.id);

  if (!collective) {
    return next(
      new ErrorResponse(`Collective not found with id of ${req.params.id}`, 404)
    );
  }

  // check if creator and updater are the same user
  if (collective.user.toString() !== req.user.id && req.user.role !== 'admin') {
    return next(
      new ErrorResponse(`User ${req.user.id} is not authorized to update this collective`, 401)
    )
  }

  collective = await Collective.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });

  res.status(200).json({
    success: true,
    data: collective
  });

});

// @desc    Delete collective
// @route   DELETE /api/collective-service/v1/collectives/:id
// @access  Private
exports.deleteCollective = asyncHandler(async (req, res, next) => {

  const collective = await Collective.findById(req.params.id);

  if (!collective) {
    return next(
      new ErrorResponse(`Collective not found with id of ${req.params.id}`, 404)
    );
  }

  // check if creator and updater are the same user
  if (collective.user.toString() !== req.user.id && req.user.role !== 'admin') {
    return next(
      new ErrorResponse(`User ${req.user.id} is not authorized to delete this collective`, 401)
    )
  }

  collective.remove();

  res.status(200).json({ success: true, data: {} });
});

// @desc    Upload photo for collective
// @route   PUT /api/collective-service/v1/collectives/:id/photo
// @access  Private
exports.collectivePhotoUpload = asyncHandler(async (req, res, next) => {

  const collective = await Collective.findById(req.params.id);

  if (!collective) {
    return next(
      new ErrorResponse(`Collective not found with id of ${req.params.id}`, 404)
    );
  }

  // check if creator and updater are the same user
  if (collective.user.toString() !== req.user.id && req.user.role !== 'admin') {
    return next(
      new ErrorResponse(`User ${req.user.id} is not authorized to update this collective`, 401)
    )
  }

  if (!req.files) {
    return next(
      new ErrorResponse(`No file selected for upload`, 400)
    );
  }

  const file = req.files.file;
  // console.log(file);
  console.log(`The file size is ${file.size}`);

  // make sure the image is a photo

  if (!file.mimetype.startsWith('image')) {
    return next(
      new ErrorResponse(`File uploaded is not an image... Please upload an image.`, 400)
    );
  }

  // check file size
  if (file.size > process.env.MAX_FILE_UPLOAD) {
    return next(
      new ErrorResponse(`File size is too big... Please upload an image with a file size smaller than ${process.env.MAX_FILE_UPLOAD}`, 400)
    );
  }

  // Create custom filename so we can avoid duplicates
  file.name = `photo_${collective._id}${path.parse(file.name).ext}`;

  file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async err => {
    if (err) {
      console.error(err);
      return next(
        new ErrorResponse(`Problem with file upload`, 500)
      );
    }

    await Collective.findByIdAndUpdate(req.params.id, { photo: file.name });
    res.status(200).json({
      success: true,
      data: file.name
    })
  });

  console.log(file.name);

});