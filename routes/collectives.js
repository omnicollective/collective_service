const express = require("express");
const {
  getCollectives,
  getCollective,
  createCollective,
  updateCollective,
  deleteCollective,
  collectivePhotoUpload,
} = require("../controllers/collectives");

const Collective = require("../models/Collective");

// Include other resource routers
// const postRouter = require("./posts");

const router = express.Router();
const advancedResults = require("../middleware/advancedResults");
const { protect, authorize } = require("../middleware/auth");

// Re-route into other resource routers
// router.use("/:collectiveId/posts", postRouter);

router
  .route("/:id/photo")
  .put(protect, authorize("user", "moderator", "admin"), collectivePhotoUpload);

// routes that don't require a specific id
router
  .route("/")
  .get(advancedResults(Collective, "posts"), getCollectives)
  .post(protect, authorize("user", "moderator", "admin"), createCollective);

// routes that require a specific id
router
  .route("/:id")
  .get(getCollective)
  .put(protect, authorize("user", "moderator", "admin"), updateCollective)
  .delete(protect, authorize("user", "moderator", "admin"), deleteCollective);

module.exports = router;
